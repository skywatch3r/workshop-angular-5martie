(function() {
  'use strict';

  angular.module('workshop-app', [
    'ngResource',
    'ngRoute',
    'ngStorage'
  ])

  .constant('urls', {
    BASE: 'https://workshop-assist.herokuapp.com/',
    BASE_API: 'https://workshop-assist.herokuapp.com/api',
  })

  function config($routeProvider, $httpProvider) {
    $routeProvider
      .when('/login', {
        templateUrl: 'app/views/login.html',
        controller: 'loginCtrl'
      })
      .when('/user', {
        templateUrl: 'app/views/user.html',
        controller: 'userCtrl'
      })
      .otherwise({ redirectTo: '/login' })
  }

  config
    .$inject = ['$routeProvider', '$httpProvider'];

  angular
    .module('workshop-app')
    .config(config)
}())
