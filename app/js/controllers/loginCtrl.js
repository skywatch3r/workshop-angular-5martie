(function() {

  'use strict';

  function loginCtrl($scope, $localStorage, $location, Auth) {
    var self = this;

    function successAuth(res) {
      $localStorage.token = res.token;
      $location.path('/user');
    }

    function errorAuth(err) {
      console.log('Err', err);
      $scope.error = 'Auth failed';
    }

    self.signin = function(email, password) {
      var formData = {
        email: email,
        password: password
      };
      Auth.signin(formData, successAuth, errorAuth);
    }

    return ($scope.lCtrl = self);
  }

  loginCtrl
    .$inject = ['$scope', '$localStorage', '$location', 'Auth'];
  angular
    .module('workshop-app')
    .controller('loginCtrl', loginCtrl)


}());
