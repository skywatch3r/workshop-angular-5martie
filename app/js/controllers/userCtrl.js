(function() {

  'use strict';

  function userCtrl($scope) {
    $scope.test = 'Multa sanatate';
  }

  userCtrl
    .$inject = ['$scope'];
  angular
    .module('workshop-app')
    .controller('userCtrl', userCtrl)
}());
